<!DOCTYPE html>
<html>
    <head>
        <title> Todo List</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="todolist.css">
    </head>
    <body>
        <div class="container">
            <center><h1>Todo List</h1></center>
            <br>
            <div class="row" style="margin-top: 40px">
                <div class="col-md-11 col-md-offset-1">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTask">Add</button>
                </div>
                <div class="col-md-1 col-md-offset-1">
                    <button type="button" class="btn btn-info" onclick="print()">Print</button>
                </div>
                <br>
            </div>
            <hr><br>
            <div id="task_data"></div>
        </div>

        <!-- Add task -->
        <div class="modal fade" id="addTask">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title ">Add task</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="" id="add_task">
                            <div class="form-group">
                                <input type="text" name="task" id="task"  class="form-control">
                            </div>
                            <br>
                            <div class="d-flex justify-content-center">
                                <button type="submit" name="submit" class="btn btn-success">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Update task  -->
        <div class="modal fade" id="updateTask">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title ">Update task</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="" id="update_task">
                            <div class="form-group">
                                <input type="text" name="updatetask" id="updatetask" class="form-control">
                            </div>
                            <br>
                            <div class="d-flex justify-content-center">
                                <button type="submit" name="submit" class="btn btn-success">Update</button>
                                <input type="hidden" id="hidden_task_id">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" type="text/javascript"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="todolist.js"></script>
    </body>
</html>
