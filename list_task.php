<?php
    include_once ('connect.php');

    // paginate
    $limit_page = 5;
    if (isset($_POST["page_number"])) {
        $page = $_POST["page_number"];
    } else {
        $page = 1;
    }
    $offset = ((int)$page - 1) * $limit_page;
    $sql = "SELECT * FROM task ORDER BY id DESC LIMIT $offset, $limit_page";
    $result = mysqli_query($connect, $sql);
    $data = "";
    if (mysqli_num_rows($result) > 0) {
        $data .= '<table class="table table-hover table-responsive-lg">
                      <thead class="thead">
                            <tr>
                              <th scope="col">No.</th>
                              <th scope="col">ID</th>
                              <th scope="col">Task</th>  
                              <th scope="col"></th>                        
                            </tr>
                      </thead>';
        while ($row = mysqli_fetch_array($result)) {
            $offset++;
            $data .=
                '<tbody>
                    <tr id="'. $row['id'] .'">                      
                        <td>'.$offset.'</td>
                        <td>'.$row['id'].'</td>
                        <td>'. $row['Task'] .'</td>                    
                        <td class="col-md-2">                         
                            <button type="button" onclick="updateTask('. $row['id'] .')" class="btn btn-success">Update</button>
                            <button type="button" onclick="deleteTask('. $row['id'] .')" class="btn btn-danger">Delete</button>
                        </td>            
                    </tr>
                </tbody>';
        }
        $data .= '</table>';
        $sql_total = "SELECT * FROM Task";
        $records = mysqli_query($connect, $sql_total);
        $total_record = mysqli_num_rows($records);
        $total_page = ceil($total_record/$limit_page);
        if ($page == 1) {
            $data .= "<div id='pagination'>
                            <nav aria-label='Page navigation example'>
                                <ul class='pagination justify-content-end'>
                                    <li class='page-item'><a class='page-link' href='#'>Previous</a></li>";
        } else {
            $i = $page - 1;
            $data .= "<div id='pagination'>
                            <nav aria-label='Page navigation example'>
                                <ul class='pagination justify-content-end'>
                                    <li class='page-item'><a class='btn btn-info' id='paginate_{$i}' onclick='paginate({$i})' href='#$i'>Previous</a></li>";
        }
        for ($i=1; $i <= $total_page; $i++) {
            $active_class = "";
            if ($i == $page) {
                $active_class = "active";
            }
            $data .= "<a id='paginate_{$i}' page_id='{$i}' class='page-item . $active_class . ' onclick='paginate({$i})' href='#$i'><span class='page-link'>$i</span></a>";
        }

        if ($page < $total_page) {
            $j = $page + 1;
            $data .= "<li class='page-item'><a class='btn btn-info' id='paginate_{$j}' onclick='paginate({$j})' href='#$j'>Next</a></li>
                        </ul>
                    </nav>";
        } else {
            $data .= "<li class='page-item'><a class='page-link' href='#'>Next</a></li>
                        </ul>
                    </nav>";
        }
        $data .= '</div>';
        echo $data;
    } else {
        echo "Không có dữ liệu!";
    }
?>