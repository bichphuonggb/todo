$(document).ready(function() {
    //add task
    $('#add_task').validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            task: "required"
        },
        messages: {
            task: "*Please enter task"
        },
        submitHandler: function(form) {
            var task = $('#task').val();
            $.ajax({
                url: 'add.php',
                type: "post",
                data: {
                    task: task
                },
                success: function (data, status) {
                    loadTask();
                    swal("Success!", "You have successfully added the task!", "success");
                    $('#add_task')[0].reset();
                    $('#addTask').modal('hide');
                }
            });
        }
    });

    // update task
    $('#update_task').validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            updatetask: "required"
        },
        messages: {
            updatetask: "*Please enter task"
        },
        submitHandler: function(form) {
            var updatetask = $('#updatetask').val();
            var hidden_task_id = $('#hidden_task_id').val();
            $.ajax({
                url: 'update.php',
                type: 'post',
                data: {
                    updatetask: updatetask,
                    hidden_task_id: hidden_task_id
                },
                success: function (data, status) {
                    loadTask();
                    swal("Success!", "You have successfully updated the task!", "success");
                    $('#updateTask').modal('hide');
                }
            });
        }
    });
});
//delete task
function deleteTask(deleteid) {
    swal({
        title: "Are you sure you want to delete?",
        text: "",
        icon: "warning",
        buttons: ["Close", "Delete"],
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: "delete.php",
                type: "post",
                data: {
                    deleteid: deleteid
                },
                success: function(data, status) {
                    $("#" + deleteid).hide("60000");
                }
            });
            swal("Delete Successfully!", {
                icon: "success",
                buttons: false,
                timer: 3000,
            });
        }
    });
}

// show update task
function updateTask(task_id) {
    $('#hidden_task_id').val(task_id);
    $.post(
        "update.php",
        {
            task_id: task_id
        },
        function(data, status) {
            var task = JSON.parse(data);
            $('#updatetask').val(task.Task);
        }
    );
    $('#updateTask').modal('show');
}
//list task
function loadTask(page) {
    $.ajax({
        url: "list_task.php",
        type: "post",
        data: {
            page_number: page
        },
        success: function(data) {
            $('#task_data').html(data);
        }
    });
}
loadTask();

// phân trang sản phẩm
function paginate(page_id) {
    loadTask(page_id);
}
